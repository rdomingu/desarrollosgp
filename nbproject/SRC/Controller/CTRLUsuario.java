/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Config.Conexion;
import Entidad.Persona;
import Entidad.Usuario;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author user
 */
@Controller
public class CTRLUsuario {
    Conexion cnx = new Conexion();
    JdbcTemplate jdbcTemplate = new JdbcTemplate(cnx.conectar());
    ModelAndView mav = new ModelAndView();
    Object id = 0;
    
    @RequestMapping("liUsuario.htm")
    public ModelAndView listar(){
        String sql =
                "select\n" +
                "	idusuario,\n" +
                "	nick,\n" +
                "	nro_intentos,\n" +
                "	estado,\n" +
                "	contrasenia,\n" +
                "	p.idpersona||'-'||p.nombres||' '||p.apellidos persona,\n" +
                "	idrol\n" +
                "from\n" +
                "	usuario u\n" +
                "	left join persona p using(idpersona)\n" +
                "order by\n" +
                "	persona";
        
        List datos = this.jdbcTemplate.queryForList(sql);
        mav.addObject("lista", datos);
        mav.setViewName("liUsuario");
        
        return mav;
    }
    
    @RequestMapping(value = "agUsuario.htm", method = RequestMethod.GET)
    public ModelAndView agregar(){
        mav.addObject(new Usuario());
        mav.setViewName("agUsuario");
        return mav;
    }
    
    @RequestMapping(value = "agUsuario.htm", method = RequestMethod.POST)
    public ModelAndView agregar(Usuario usuario){
        String sql = 
            "insert into\n" +
            "	usuario\n" +
            "(\n" +
            "nick,\n" +
            "contrasenia,\n" +
            "idpersona,\n" +
            "idrol\n" +
            ")\n" +
            "values\n" +
            "(\n" +
            "?,\n" +
            "?,\n" +
            "?,\n" +
            "?\n" +
            ")";
        this.jdbcTemplate.update(sql,
                usuario.getNick(),
                usuario.getContrasenia(),
                usuario.getIdpersona(),
                usuario.getIdrol()
                );
        return new ModelAndView("redirect:/liUsuario.htm");
    }
    
    @RequestMapping(value = "edUsuario.htm", method = RequestMethod.GET)
    public ModelAndView editar(HttpServletRequest request){
        id = request.getParameter("id");
        String sql = 
                "select\n" +
                "	idusuario,\n" +
                "	nick,\n" +
                "	contrasenia,\n" +
                "	idpersona,\n" +
                "	idrol\n" +
                "from\n" +
                "	usuario u\n"+
                "where\n" +
                "	idusuario="+id;
        List datos = this.jdbcTemplate.queryForList(sql);
        mav.addObject("lista",datos);
        mav.setViewName("edUsuario");
        return mav;
    }
    
    @RequestMapping(value="edUsuario.htm", method = RequestMethod.POST)
    public ModelAndView editar(Usuario usuario){
        
        String sql = 
                "update\n" +
                "	usuario\n" +
                "set\n" +
                "	nick=?,\n" +
                "	contrasenia=?,\n" +
                "	idpersona=?,\n" +
                "	idrol=?\n" +
                "where\n" +
                "	idusuario="+id;
        this.jdbcTemplate.update(sql,
                usuario.getNick(),
                usuario.getContrasenia(),
                usuario.getIdpersona(),
                usuario.getIdrol()
                );        
        
        return new ModelAndView("redirect:/liUsuario.htm");
    }
    
    @RequestMapping("elUsuario.htm")
    public ModelAndView delete(HttpServletRequest request){
        id = request.getParameter("id");
        String sql = "delete from usuario where idusuario="+id;
        this.jdbcTemplate.update(sql);        
        
        return new ModelAndView("redirect:/liUsuario.htm");
    }
}

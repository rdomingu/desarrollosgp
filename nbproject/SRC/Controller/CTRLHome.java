/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Config.Conexion;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author user
 */
public class CTRLHome {    
    Conexion cnx = new Conexion();
    JdbcTemplate jdbcTemplate = new JdbcTemplate(cnx.conectar());
    ModelAndView mav = new ModelAndView();
    Object id = 0;        
    
    @RequestMapping(value = "index.htm")
    public ModelAndView login(){
        //mav.addObject();
        mav.setViewName("index");
        return mav;
    }
    
    @RequestMapping(value = "principal.htm", method = RequestMethod.GET)
    public ModelAndView principal(){
        
        mav.setViewName("principal");
        return mav;
    }         
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidad;

/**
 *
 * @author user
 */
public class Usuario {
    private Object nick;
    private Object contrasenia;
    private int nro_intentos;
    private int idpersona;
    private int idrol;

    public void setNick(Object nick) {
        this.nick = nick;
    }

    public void setContrasenia(Object contrasenia) {
        this.contrasenia = contrasenia;
    }

    public void setNro_intentos(int nro_intentos) {
        this.nro_intentos = nro_intentos;
    }

    public void setIdpersona(int idpersona) {
        
        this.idpersona = idpersona;
    }

    public void setIdrol(int idrol) {
        this.idrol = idrol;
    }

    public Object getNick() {
        return nick;
    }

    public Object getContrasenia() {
        return contrasenia;
    }

    public int getNro_intentos() {
        return nro_intentos;
    }

    public Object getIdpersona() {        
        return idpersona;
    }

    public Object getIdrol() {
        return idrol;
    }
    
    
    
}
